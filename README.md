# gtk_scaling_app

App for mobian to make scaling certain apps easier as well as adding the option to change themes. To run the program right now just clone the repo then `cd` into the directory then run the script with `python3 scaling_gtk.py`.