import subprocess as sp
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


# ---------------------------- Backend Code ---------------------------------------
scale = "scale-to-fit"
"""
Need to also make functions or edit functions to make them turn off.
"""

def firefox_scale():
    sp.call(f"{scale} firefox-esr on")

def calendar_scale():
    sp.call(f"{scale} gnome-calendar on")

def cawbird_scale():
    sp.call(f"{scale} cawbird on")

def empathy_scale():
    sp.call(f"{scale} empathy on")

def geary_scale():
    sp.call(f"{scale} geary on")

def map_scale():
    sp.call(f"{scale} org.gnome.Maps on")

def mumble_scale():
    sp.call(f"{scale} net.sourceforge.mumble.mumble on")

def nautilus_scale():
    sp.call(f"{scale} org.gnome.nautilus on")

def totem_scale():
    sp.call(f"{scale} totem on")

def sudoku_scale():
    sp.call(f"{scale} gnome-sudoku on")

def enable_dark_theme():
    dark_theme = "gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'"
    sp.call(f"{dark_theme}")

def main():
    firefox_scale()
    calendar_scale()
    cawbird_scale()
    empathy_scale()
    geary_scale()
    map_scale()
    mumble_scale()
    nautilus_scale()
    totem_scale()
    sudoku_scale()
    enable_dark_theme()
    


# ---------------------------- Front end code --------------------------------------
window = Gtk.Window()
# -------------------------------- Buttons ---------------------------------------
firefox_button = Gtk.Button(label = "Firefox scaling")
firefox_button.connect("clicked", firefox_scale)
add(firefox_button)




# -------------------------------- Other Stuff -------------------------------------
window.connect("destroy", Gtk.main_quit)
window.show_all()
Gtk.main()
